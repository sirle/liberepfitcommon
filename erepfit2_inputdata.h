#ifndef EREPFIT2_INPUTDATA
#define EREPFIT2_INPUTDATA

#include <QDir>
#include <QFileInfo>
#include <QMap>
#include <QString>
#include <QTextStream>
#include <variantqt.h>

#include <QDataStream>

#include "basic_types.h"

namespace Erepfit2
{
namespace Input
{

    class ToolChain
    {
    public:
        QString name = "dftb+";
        QString path = "dftb+";
        QVariantMap options;
    };

    class PotentialGrid
    {
    public:
        QList<double> knots;
    };

    class Option
    {
    public:
        ToolChain toolchain;
        bool FitAllAtomEnergy = false;
        bool debug = false;
        int spline_order = 4;
        int continous_order = 3;
        bool use_cache = false;
    };

    class EnergyEquation
    {
    public:
        QString name;
        ADPT::UUID uuid;
        double energy;
        double weight = 1.0;
        QString unit = "kcal/mol";
    };

    class ForceEquation
    {
    public:
        QString name;
        ADPT::UUID uuid;
        double weight = 1.0;
        bool has_reference_force = false;
        QList<dVector3> reference_force;
    };

    class AdditionalEquation
    {
    public:
        QString potential;
        double distance;
        int derivative;
        double value;
        double weight = 1.0;
        QString unit = "bohr";
    };

    class ReactionItem
    {
    public:
        QString name;
        ADPT::UUID uuid;
        double coefficient;
    };

    class ReactionEquation
    {
    public:
        QList<ReactionItem> reactants;
        QList<ReactionItem> products;
        double weight = 1.0;
        double energy;
        QString name;
        ADPT::UUID uuid;
        QString unit = "kcal/mol";
    };

    class Coordinate
    {
    public:
        QString symbol;
        dVector3 coord;
    };

    class Geometry
    {
    public:
        bool loadGen(const QString& filename);
        bool loadGen(QTextStream& fin);
        bool loadXYZ(const QString& filename);
        QStringList getElements() const;
        QStringList getAtoms() const;
        QString getGen() const;
        QString getXYZ() const;
        QString getDCDFTBKXYZ() const;
        QString getOldVersionXYZ() const;
        dVector3 getCoordinate(int index) const;
        void reset();

    public:
        //Crystal
        dMatrix3x3 lattice_vectors;
        double scaling_factor = 1.0;

        ADPT::KPointsSetting kpoints;

        bool fractional_coordinates = true;

        //Molecule
        int charge = 0;
        int spin = 1;

        //Common
        QList<Coordinate> coordinates;

        bool isPBC = false;
        bool spin_polarization = false;
        double unpaired_electrons = 0.0;
    };

    class SystemElectronicData
    {
    public:
        QList<dVector3> force;
        bool pbc = false;
        dMatrix3x3 stress;
        double energy = 0.0;
        double efermi_a = 0.0;
        double efermi_b = 0.0;
    };

    class System
    {
    public:
        Geometry geometry;
        QString template_input;
        QString name;
        ADPT::UUID uuid;
        bool evaluated = false;
        SystemElectronicData elec_data;
        bool opt_upe = false;
    };

    class AtomicEnergy
    {
    public:
        QMap<QString, QString> atomic_dftbinputs;
        QMap<QString, double> atomic_energy;
        bool isAtomDefined(const QString& symbol) const
        {
            if (atomic_energy.contains(symbol) || atomic_dftbinputs.contains(symbol))
            {
                return true;
            }
            return false;
        }
    };

    class Equation
    {
    public:
        QList<EnergyEquation> energyEquations;
        QList<ForceEquation> forceEquations;
        QList<ReactionEquation> reactionEquations;
        QMap<QString, QList<AdditionalEquation>> additionalEquations;
    };

    class Erepfit2_InputData
    {
    public:
        Erepfit2_InputData() = default;
        Erepfit2_InputData(const Erepfit2_InputData& rhs);

        AtomicEnergy atomicEnergy;
        QMap<QString, PotentialGrid> potential_grids;
        Equation equations;
        QList<System> systems;
        Option options;
        QVariantMap dftb_options;

        std::shared_ptr<ADPT::SKFileInfo> electronic_skfiles;
        QMap<QString, QString> external_repulsive_potentials;

    public:
        QSet<QString> required_elec_pairs() const;
        QStringList required_elements() const;
        void makeAbsolutePath(const QDir& rootDir);
        void symmetrize_external_repulsive_potentials();
    };
}
}
QDataStream& operator<<(QDataStream& dataStream, const Erepfit2::Input::SystemElectronicData& molecule);
QDataStream& operator>>(QDataStream& dataStream, Erepfit2::Input::SystemElectronicData& molecule);

#endif // EREPFIT2_INPUTDATA
