#ifndef EREPFIT2_INPUTDATA_JSON_YAML
#define EREPFIT2_INPUTDATA_JSON_YAML

#include "erepfit2_inputdata.h"

namespace libvariant
{

template <>
struct VariantConvertor<Erepfit2::Input::Coordinate>
{
    static libvariant::Variant toVariant(const Erepfit2::Input::Coordinate& rhs)
    {
        Variant var;
        var.Append(rhs.symbol.toStdString());
        for (int i = 0; i < 3; ++i)
        {
            var.Append(rhs.coord[i]);
        }
        return var;
    }
    static void fromVariant(const libvariant::Variant& var, Erepfit2::Input::Coordinate& rhs)
    {
        GET_VARIANT_LIST(rhs.symbol, var, 0, QString);
        for (int i = 0; i < 3; ++i)
        {
            GET_VARIANT_LIST(rhs.coord[i], var, 1 + i, double);
        }
    }
};

template <>
struct VariantConvertor<Erepfit2::Input::Geometry>
{
    static libvariant::Variant toVariant(const Erepfit2::Input::Geometry& rhs)
    {
        Variant var;
        var["coordinates"] = VariantConvertor<QList<Erepfit2::Input::Coordinate>>::toVariant(rhs.coordinates);
        if (rhs.isPBC)
        {
            var["kpoints"] = VariantConvertor<ADPT::KPointsSetting>::toVariant(rhs.kpoints);
            var["lattice_vectors"] = VariantConvertor<dMatrix3x3>::toVariant(rhs.lattice_vectors);
            var["fractional_coordinates"] = rhs.fractional_coordinates;
            var["scaling_factor"] = rhs.scaling_factor;
        }
        else
        {
            var["charge"] = rhs.charge;
            var["spinpol"] = rhs.spin_polarization;
            if (rhs.isPBC)
            {
                var["upe"] = rhs.unpaired_electrons;
            }
            else
            {
                var["spin"] = rhs.spin;
            }
        }
        return var;
    }
    static void fromVariant(const libvariant::Variant& var, Erepfit2::Input::Geometry& rhs)
    {
        bool hasValue;

//        bool isLoaded = false;
//        QString filename;
//        GET_VARIANT_OPT(filename, var, "coordinates_gen", QString, hasValue);
//        if (hasValue)
//        {
//            if (!rhs.loadGen(filename))
//            {
//                throw KeywordParseError("coordinates_gen:" + filename.toStdString());
//            }
//            isLoaded = true;
//        }
//        if (!isLoaded)
//        {
//            GET_VARIANT_OPT(filename, var, "coordinates_xyz", QString, hasValue);
//            if (hasValue)
//            {
//                if (!rhs.loadXYZ(filename))
//                {
//                    throw KeywordParseError("coordinates_xyz:" + filename.toStdString());
//                }
//                isLoaded = true;
//            }
//        }
//        if (!isLoaded)
//        {
            GET_VARIANT(rhs.coordinates, var, "coordinates", QList<Erepfit2::Input::Coordinate>);
//        }

        GET_VARIANT_OPT(rhs.spin_polarization, var, "spinpol", bool, hasValue);
        GET_VARIANT_OPT(rhs.lattice_vectors, var, "lattice_vectors", dMatrix3x3, rhs.isPBC);
        if (rhs.isPBC)
        {

            GET_VARIANT_OPT(rhs.scaling_factor, var, "scaling_factor", double, hasValue);
            GET_VARIANT(rhs.kpoints, var, "kpoints", ADPT::KPointsSetting);
            GET_VARIANT(rhs.fractional_coordinates, var, "fractional_coordinates", bool);
            if (rhs.spin_polarization)
            {
                GET_VARIANT(rhs.unpaired_electrons, var, "upe", double);
            }
        }
        else
        {
            GET_VARIANT_OPT(rhs.charge, var, "charge", int, hasValue);

            if (rhs.spin_polarization)
            {
                GET_VARIANT(rhs.spin, var, "spin", int);
            }
        }
    }
};

template <>
struct VariantConvertor<Erepfit2::Input::SystemElectronicData>
{
    static libvariant::Variant toVariant(const Erepfit2::Input::SystemElectronicData& rhs)
    {
        Variant var;
        var["energy"] = rhs.energy;
        var["force"] = VariantConvertor<QList<dVector3>>::toVariant(rhs.force);
        if (rhs.pbc)
        {
            var["pbc"] = rhs.pbc;
            var["stress"] = VariantConvertor<dMatrix3x3>::toVariant(rhs.stress);
        }
        return var;
    }
    static void fromVariant(const libvariant::Variant& var, Erepfit2::Input::SystemElectronicData& rhs)
    {
        bool hasValue;
        Q_UNUSED(hasValue)
        GET_VARIANT(rhs.energy, var, "energy", double);

        GET_VARIANT(rhs.force, var, "force", QList<dVector3>);

        GET_VARIANT_OPT(rhs.pbc, var, "pbc", bool, hasValue);

        if (rhs.pbc)
        {
            GET_VARIANT(rhs.stress, var, "stress", dMatrix3x3);
        }
    }
};

template <>
struct VariantConvertor<Erepfit2::Input::System>
{
    static libvariant::Variant toVariant(const Erepfit2::Input::System& rhs)
    {
        Variant var;
        var["geometry"] = VariantConvertor<Erepfit2::Input::Geometry>::toVariant(rhs.geometry);
        var["name"] = rhs.name.toStdString();
        var["uuid"] = VariantConvertor<ADPT::UUID>::toVariant(rhs.uuid);

        if (rhs.evaluated)
        {
            var["evaluated"] = rhs.evaluated;
            var["elec_data"] = VariantConvertor<Erepfit2::Input::SystemElectronicData>::toVariant(rhs.elec_data);
        }
        else
        {
            var["template_input"] = rhs.template_input.toStdString();
        }

        if (rhs.geometry.isPBC && rhs.geometry.spin_polarization)
        {
            var["upe"] = rhs.opt_upe;
        }

        return var;
    }
    static void fromVariant(const libvariant::Variant& var, Erepfit2::Input::System& rhs)
    {
        bool hasValue;
        Q_UNUSED(hasValue)
        GET_VARIANT(rhs.uuid, var, "uuid", ADPT::UUID);
        GET_VARIANT(rhs.geometry, var, "geometry", Erepfit2::Input::Geometry);
        GET_VARIANT_OPT(rhs.name, var, "name", QString, hasValue);

        GET_VARIANT_OPT(rhs.evaluated, var, "evaluated", bool, hasValue);

        GET_VARIANT_OPT(rhs.opt_upe, var, "opt_upe", bool, hasValue);

        if (rhs.evaluated)
        {
            GET_VARIANT(rhs.elec_data, var, "elec_data", Erepfit2::Input::SystemElectronicData);
        }
        else
        {
            GET_VARIANT(rhs.template_input, var, "template_input", QString);
        }
    }
};

template <>
struct VariantConvertor<Erepfit2::Input::PotentialGrid>
{
    static libvariant::Variant toVariant(const Erepfit2::Input::PotentialGrid& rhs)
    {
        Variant var;
        var["knots"] = VariantConvertor<QList<double>>::toVariant(rhs.knots);
        return var;
    }
    static void fromVariant(const libvariant::Variant& var, Erepfit2::Input::PotentialGrid& rhs)
    {
        GET_VARIANT(rhs.knots, var, "knots", QList<double>)
    }
};

template <>
struct VariantConvertor<Erepfit2::Input::EnergyEquation>
{
    static libvariant::Variant toVariant(const Erepfit2::Input::EnergyEquation& rhs)
    {
        Variant var;
        var["name"] = rhs.name.toStdString();
        var["energy"] = rhs.energy;
        var["weight"] = rhs.weight;
        var["uuid"] = VariantConvertor<ADPT::UUID>::toVariant(rhs.uuid);
        var["unit"] = rhs.unit.toStdString();
        return var;
    }
    static void fromVariant(const libvariant::Variant& var, Erepfit2::Input::EnergyEquation& rhs)
    {
        GET_VARIANT(rhs.uuid, var, "uuid", ADPT::UUID);
        GET_VARIANT(rhs.energy, var, "energy", double);
        GET_VARIANT(rhs.unit, var, "unit", QString);

        bool hasValue;
        GET_VARIANT_OPT(rhs.name, var, "name", QString, hasValue);
        GET_VARIANT_OPT(rhs.weight, var, "weight", double, hasValue);

        Q_UNUSED(hasValue);
    }
};

template <>
struct VariantConvertor<Erepfit2::Input::ForceEquation>
{
    static libvariant::Variant toVariant(const Erepfit2::Input::ForceEquation& rhs)
    {
        Variant var;
        var["name"] = rhs.name.toStdString();
        var["weight"] = rhs.weight;
        if (rhs.has_reference_force)
        {
            var["reference_force"] = VariantConvertor<QList<dVector3>>::toVariant(rhs.reference_force);
        }
        var["uuid"] = VariantConvertor<ADPT::UUID>::toVariant(rhs.uuid);
        return var;
    }
    static void fromVariant(const libvariant::Variant& var, Erepfit2::Input::ForceEquation& rhs)
    {
        bool hasValue;
        GET_VARIANT_OPT(rhs.name, var, "name", QString, hasValue);
        GET_VARIANT_OPT(rhs.weight, var, "weight", double, hasValue);

        GET_VARIANT(rhs.uuid, var, "uuid", ADPT::UUID);

        GET_VARIANT_OPT(rhs.reference_force, var, "reference_force", QList<dVector3>, rhs.has_reference_force);
        Q_UNUSED(hasValue);
    }
};

template <>
struct VariantConvertor<Erepfit2::Input::ReactionItem>
{
    static Variant toVariant(const Erepfit2::Input::ReactionItem& rhs)
    {
        Variant var;
        var["coefficient"] = rhs.coefficient;
        var["name"] = rhs.name.toStdString();
        var["uuid"] = VariantConvertor<ADPT::UUID>::toVariant(rhs.uuid);
        return var;
    }
    static void fromVariant(const Variant& var, Erepfit2::Input::ReactionItem& rhs)
    {
        GET_VARIANT(rhs.uuid, var, "uuid", ADPT::UUID);
        GET_VARIANT(rhs.coefficient, var, "coefficient", double);

        bool hasValue;
        GET_VARIANT_OPT(rhs.name, var, "name", QString, hasValue);

        Q_UNUSED(hasValue);
    }
};

template <>
struct VariantConvertor<Erepfit2::Input::ReactionEquation>
{
    static Variant toVariant(const Erepfit2::Input::ReactionEquation& rhs)
    {
        Variant var;
        var["energy"] = rhs.energy;
        var["reactants"] = VariantConvertor<QList<Erepfit2::Input::ReactionItem>>::toVariant(rhs.reactants);
        var["products"] = VariantConvertor<QList<Erepfit2::Input::ReactionItem>>::toVariant(rhs.products);
        var["weight"] = rhs.weight;
        var["unit"] = rhs.unit.toStdString();
        var["name"] = rhs.name.toStdString();
        var["uuid"] = VariantConvertor<ADPT::UUID>::toVariant(rhs.uuid);
        return var;
    }
    static void fromVariant(const Variant& var, Erepfit2::Input::ReactionEquation& rhs)
    {
        GET_VARIANT(rhs.energy, var, "energy", double);
        GET_VARIANT(rhs.unit, var, "unit", QString);
        GET_VARIANT(rhs.uuid, var, "uuid", ADPT::UUID);

        GET_VARIANT(rhs.reactants, var, "reactants", QList<Erepfit2::Input::ReactionItem>);
        GET_VARIANT(rhs.products, var, "products", QList<Erepfit2::Input::ReactionItem>);

        bool hasValue;
        GET_VARIANT_OPT(rhs.weight, var, "weight", double, hasValue);
        GET_VARIANT_OPT(rhs.name, var, "name", QString, hasValue);

        Q_UNUSED(hasValue);
    }
};

template <>
struct VariantConvertor<Erepfit2::Input::ToolChain>
{
    static libvariant::Variant toVariant(const Erepfit2::Input::ToolChain& rhs)
    {
        Variant var;
        var["name"] = VariantConvertor<QString>::toVariant(rhs.name);
        var["path"] = VariantConvertor<QString>::toVariant(rhs.path);
        if (!rhs.options.isEmpty())
        {
            var["options"] = VariantConvertor<QVariantMap>::toVariant(rhs.options);
        }
        return var;
    }
    static void fromVariant(const libvariant::Variant& var, Erepfit2::Input::ToolChain& rhs)
    {
        GET_VARIANT(rhs.name, var, "name", QString);
        GET_VARIANT(rhs.path, var, "path", QString);
        bool hasValue;
        Q_UNUSED(hasValue)
        GET_VARIANT_OPT(rhs.options, var, "options", QVariantMap, hasValue);
    }
};

template <>
struct VariantConvertor<Erepfit2::Input::Option>
{
    static libvariant::Variant toVariant(const Erepfit2::Input::Option& rhs)
    {
        Variant var;
        var["spline_order"] = rhs.spline_order;
        var["toolchain"] = VariantConvertor<Erepfit2::Input::ToolChain>::toVariant(rhs.toolchain);
        var["fit_all_atom_energy"] = rhs.FitAllAtomEnergy;
        var["debug"] = rhs.debug;
        var["use_cache"] = rhs.use_cache;
        var["continous_order"] = rhs.continous_order;
        return var;
    }
    static void fromVariant(const libvariant::Variant& var, Erepfit2::Input::Option& rhs)
    {
        GET_VARIANT(rhs.toolchain, var, "toolchain", Erepfit2::Input::ToolChain);

        bool hasValue;

        GET_VARIANT_OPT(rhs.spline_order, var, "spline_order", int, hasValue);
        GET_VARIANT_OPT(rhs.FitAllAtomEnergy, var, "fit_all_atom_energy", bool, hasValue);
        GET_VARIANT_OPT(rhs.debug, var, "debug", bool, hasValue);
        GET_VARIANT_OPT(rhs.use_cache, var, "use_cache", bool, hasValue);
        GET_VARIANT_OPT(rhs.continous_order, var, "continous_order", int, hasValue);
        Q_UNUSED(hasValue);
    }
};

template <>
struct VariantConvertor<Erepfit2::Input::AtomicEnergy>
{
    static Variant toVariant(const Erepfit2::Input::AtomicEnergy& rhs)
    {
        std::map<std::string, Variant> var;
        {
            QMapIterator<QString, QString> it(rhs.atomic_dftbinputs);
            while (it.hasNext())
            {
                it.next();
                var[it.key().toStdString()] = it.value().toStdString();
            }
        }
        {
            QMapIterator<QString, double> it(rhs.atomic_energy);
            while (it.hasNext())
            {
                it.next();
                var[it.key().toStdString()] = it.value();
            }
        }
        return var;
    }
    static void fromVariant(const Variant& var, Erepfit2::Input::AtomicEnergy& rhs)
    {
        try
        {
            for (auto it = var.MapBegin(); it != var.MapEnd(); ++it)
            {
                QString key = QString::fromStdString(it->first);
                QVariant tmp = QVariant(QString::fromStdString(it->second.AsString()));
                bool ok;
                double value = tmp.toDouble(&ok);
                if (ok)
                {
                    rhs.atomic_energy.insert(key, value);
                }
                else
                {
                    rhs.atomic_dftbinputs.insert(key, tmp.toString());
                }
            }
        }
        catch (const std::exception& e)
        {
            std::cout << e.what() << std::endl;
            throw e;
        }
    }
};

template <>
struct VariantConvertor<Erepfit2::Input::AdditionalEquation>
{
    static Variant toVariant(const Erepfit2::Input::AdditionalEquation& rhs)
    {
        Variant var;
        var["derivative"] = rhs.derivative;
        var["distance"] = rhs.distance;
        var["unit"] = rhs.unit.toStdString();
        var["value"] = rhs.value;
        var["weight"] = rhs.weight;
        return var;
    }
    static void fromVariant(const Variant& var, Erepfit2::Input::AdditionalEquation& rhs)
    {
        try
        {
            rhs.derivative = var["derivative"].AsDouble();
            rhs.distance = var["distance"].AsDouble();
            rhs.unit = QString::fromStdString(var["unit"].AsString()).toLower();
            rhs.value = var["value"].AsDouble();
            bool hasValue;
            GET_VARIANT_OPT(rhs.weight, var, "weight", double, hasValue);
            Q_UNUSED(hasValue);
        }
        catch (const std::exception& e)
        {
            std::cout << e.what() << std::endl;
            throw e;
        }
    }
};

template <>
struct VariantConvertor<Erepfit2::Input::Equation>
{
    static libvariant::Variant toVariant(const Erepfit2::Input::Equation& rhs)
    {
        Variant var;
        var["reaction"] = VariantConvertor<QList<Erepfit2::Input::ReactionEquation>>::toVariant(rhs.reactionEquations);
        var["energy"] = VariantConvertor<QList<Erepfit2::Input::EnergyEquation>>::toVariant(rhs.energyEquations);
        var["force"] = VariantConvertor<QList<Erepfit2::Input::ForceEquation>>::toVariant(rhs.forceEquations);
        var["additional"] = VariantConvertor<QMap<QString, QList<Erepfit2::Input::AdditionalEquation>>>::toVariant(rhs.additionalEquations);
        return var;
    }
    static void fromVariant(const libvariant::Variant& var, Erepfit2::Input::Equation& rhs)
    {
        bool hasValue;
        typedef QMap<QString, QList<Erepfit2::Input::AdditionalEquation>> mpp;
        GET_VARIANT_OPT(rhs.reactionEquations, var, "reaction", QList<Erepfit2::Input::ReactionEquation>, hasValue);
        GET_VARIANT_OPT(rhs.energyEquations, var, "energy", QList<Erepfit2::Input::EnergyEquation>, hasValue);
        GET_VARIANT_OPT(rhs.forceEquations, var, "force", QList<Erepfit2::Input::ForceEquation>, hasValue);
        GET_VARIANT_OPT(rhs.additionalEquations, var, "additional", mpp, hasValue);

        Q_UNUSED(hasValue);
    }
};

template <>
struct VariantConvertor<Erepfit2::Input::Erepfit2_InputData>
{
    static libvariant::Variant toVariant(const Erepfit2::Input::Erepfit2_InputData& rhs)
    {
        Variant var;
        var["equations"] = VariantConvertor<Erepfit2::Input::Equation>::toVariant(rhs.equations);
        if (!rhs.equations.energyEquations.isEmpty())
        {
            var["atomic_energy"] = VariantConvertor<Erepfit2::Input::AtomicEnergy>::toVariant(rhs.atomicEnergy);
        }

        if (rhs.electronic_skfiles)
        {
            var["electronic_slater_koster_files"] = VariantConvertor<ADPT::SKFileInfo>::toVariant(*rhs.electronic_skfiles);
        }

        var["options"] = VariantConvertor<Erepfit2::Input::Option>::toVariant(rhs.options);
        var["potential_grids"] = VariantConvertor<QMap<QString, Erepfit2::Input::PotentialGrid>>::toVariant(rhs.potential_grids);
        var["systems"] = VariantConvertor<QList<Erepfit2::Input::System>>::toVariant(rhs.systems);

        var["external_repulsive_potentials"] = VariantConvertor<QMap<QString, QString>>::toVariant(rhs.external_repulsive_potentials);
        if (!rhs.dftb_options.isEmpty())
        {
            var["dftb_options"] = VariantConvertor<QVariantMap>::toVariant(rhs.dftb_options);
        }
        return var;
    }
    static void fromVariant(const libvariant::Variant& var, Erepfit2::Input::Erepfit2_InputData& rhs)
    {
        GET_VARIANT(rhs.systems, var, "systems", QList<Erepfit2::Input::System>);
        GET_VARIANT(rhs.equations, var, "equations", Erepfit2::Input::Equation);

        using map_s_pg = QMap<QString, Erepfit2::Input::PotentialGrid>;
        GET_VARIANT(rhs.potential_grids, var, "potential_grids", map_s_pg);

        bool hasValue;
        GET_VARIANT_OPT(rhs.atomicEnergy, var, "atomic_energy", Erepfit2::Input::AtomicEnergy, hasValue);
        GET_VARIANT_PTR_OPT(rhs.electronic_skfiles, var, "electronic_slater_koster_files", ADPT::SKFileInfo, hasValue);
        typedef QMap<QString, QString> mss;
        GET_VARIANT_OPT(rhs.external_repulsive_potentials, var, "external_repulsive_potentials", mss, hasValue);
        rhs.symmetrize_external_repulsive_potentials();
        GET_VARIANT_OPT(rhs.options, var, "options", Erepfit2::Input::Option, hasValue);

        Q_UNUSED(hasValue);
        GET_VARIANT_OPT(rhs.dftb_options, var, "dftb_options", QVariantMap, hasValue);
    }
};
}

#endif // EREPFIT2_INPUTDATA_JSON_YAML
