#include "erepfit2_inputdata.h"
#include <QDir>
#include <QFile>
#include <QSet>
#include <QString>
#include <QTextStream>
#include <array>

#include "atomicproperties.h"
#include "utils.h"
#include <QStringBuilder>
#include "process_utils.h"

namespace Erepfit2
{
namespace Input
{

    bool Geometry::loadGen(const QString& filename)
    {
        QFile file(filename);
        if (file.open(QIODevice::Text | QIODevice::ReadOnly))
        {
            QTextStream fin(&file);
            return loadGen(fin);
        }
        return false;
    }

    bool Geometry::loadGen(QTextStream& fin)
    {
        reset();
        try
        {
            QString line = fin.readLine();
            QStringList arr = line.split(QRegExp("\\s+"), QString::SkipEmptyParts);
            if (arr[1].toLower() == "c")
            {
                this->isPBC = false;
            }
            else if (arr[1].toLower() == "s")
            {
                this->isPBC = true;
                this->fractional_coordinates = false;
            }
            else if (arr[1].toLower() == "f")
            {
                this->isPBC = true;
                this->fractional_coordinates = true;
            }

            line = fin.readLine();
            QStringList symbols = line.split(QRegExp("\\s+"), QString::SkipEmptyParts);
            int nat = arr[0].toInt();
            for (int i = 0; i < nat; ++i)
            {
                line = fin.readLine();
                QStringList arr = line.split(QRegExp("\\s+"), QString::SkipEmptyParts);
                if (arr.size() < 5)
                {
                    return false;
                }
                int index = arr[1].toInt();
                double x, y, z;
                x = arr[2].toDouble();
                y = arr[3].toDouble();
                z = arr[4].toDouble();
                Coordinate coord;
                coord.symbol = symbols[index - 1];
                coord.coord[0] = x;
                coord.coord[1] = y;
                coord.coord[2] = z;
                this->coordinates.append(coord);
            }

            if (this->isPBC)
            {
                this->scaling_factor = 1.0;
                line = fin.readLine();
                for (int i = 0; i < 3; ++i)
                {
                    line = fin.readLine();
                    QStringList arr = line.split(QRegExp("\\s+"), QString::SkipEmptyParts);
                    double x, y, z;
                    x = arr[0].toDouble();
                    y = arr[1].toDouble();
                    z = arr[2].toDouble();

                    this->lattice_vectors[i][0] = x;
                    this->lattice_vectors[i][1] = y;
                    this->lattice_vectors[i][2] = z;
                }
            }
            return true;
        }
        catch (...)
        {
            return false;
        }
    }

    bool Geometry::loadXYZ(const QString& filename)
    {
        reset();
        QFile file(filename);
        if (file.open(QIODevice::Text | QIODevice::ReadOnly))
        {
            QTextStream fin(&file);
            QString line = fin.readLine();
            QStringList arr = line.split(" ", QString::SkipEmptyParts);
            fin.readLine();

            int nat = arr[0].toInt();
            for (int i = 0; i < nat; ++i)
            {
                line = fin.readLine();
                QStringList arr = line.split(QRegExp("\\s+"), QString::SkipEmptyParts);
                double x, y, z;
                x = arr[1].toDouble();
                y = arr[2].toDouble();
                z = arr[3].toDouble();
                Coordinate coord;
                coord.symbol = arr[0];
                coord.coord[0] = x;
                coord.coord[1] = y;
                coord.coord[2] = z;
                this->coordinates.append(coord);
            }

            this->isPBC = false;
            if (!fin.atEnd())
            {
                for (int i = 0; i < 3; ++i)
                {
                    line = fin.readLine();
                    QStringList arr = line.split(QRegExp("\\s+"), QString::SkipEmptyParts);
                    if (arr[0].toLower() == "tv")
                    {
                        this->isPBC = true;
                        this->fractional_coordinates = false;
                        this->scaling_factor = 1.0;

                        double x, y, z;
                        x = arr[1].toDouble();
                        y = arr[2].toDouble();
                        z = arr[3].toDouble();

                        this->lattice_vectors[i][0] = x;
                        this->lattice_vectors[i][1] = y;
                        this->lattice_vectors[i][2] = z;
                    }
                }
            }
            return true;
        }
        return false;
    }

    QStringList Geometry::getElements() const
    {
        QStringList res;
        for (int i = 0; i < this->coordinates.size(); ++i)
        {
            res.append(this->coordinates[i].symbol);
        }
        res.removeDuplicates();
        return res;
    }

    QStringList Geometry::getAtoms() const
    {
        QStringList res;
        for (int i = 0; i < this->coordinates.size(); ++i)
        {
            res.append(this->coordinates[i].symbol);
        }
        return res;
    }

    QString Geometry::getGen() const
    {
        QStringList res;
        if (this->isPBC)
        {
            if (this->fractional_coordinates)
            {
                res << QString("%1 F").arg(coordinates.size());
            }
            else
            {
                res << QString("%1 S").arg(coordinates.size());
            }
        }
        else
        {
            res << QString("%1 C").arg(coordinates.size());
        }

        QStringList elements;
        for (int i = 0; i < coordinates.size(); ++i)
        {
            const ADPT::AtomicProperties* elem = ADPT::AtomicProperties::fromSymbol(coordinates[i].symbol);
            elements.append(elem->getSymbol());
        }

        elements.removeDuplicates();
        res << elements.join(" ");
        for (int i = 0; i < coordinates.size(); ++i)
        {
            const ADPT::AtomicProperties* elem = ADPT::AtomicProperties::fromSymbol(coordinates[i].symbol);
            res << QString("%1 %2 %3 %4 %5").arg(i + 1).arg(elements.indexOf(elem->getSymbol()) + 1).arg(coordinates[i].coord[0], 20, 'f', 12).arg(coordinates[i].coord[1], 20, 'f', 12).arg(coordinates[i].coord[2], 20, 'f', 12);
        }
        if (isPBC)
        {
            res.append("0.0 0.0 0.0");
            std::array<std::array<double, 3>, 3> vec;
            for (int row = 0; row < 3; ++row)
            {
                for (int col = 0; col < 3; ++col)
                {
                    vec[row][col] = scaling_factor * lattice_vectors[row][col];
                }
                res.append(QString("%1 %2 %3")
                               .arg(vec[row][0], 20, 'f', 12)
                               .arg(vec[row][1], 20, 'f', 12)
                               .arg(vec[row][2], 20, 'f', 12));
            }
        }
        return res.join("\n");
    }

    QString Geometry::getDCDFTBKXYZ() const
    {
        QStringList res;
        res.append(QString("%1 %2 %3").arg(QString::number(coordinates.size())).arg(charge).arg(spin));

        for (int i = 0; i < coordinates.size(); ++i)
        {
            auto coords = getCoordinate(i);
            res.append(QString("%1 %2 %3 %4").arg(coordinates.at(i).symbol, -2).arg(coords[0], 14, 'f', 8).arg(coords[1], 14, 'f', 8).arg(coords[2], 14, 'f', 8));
        }

        if (this->isPBC)
        {
            for (int row = 0; row < 3; ++row)
            {
                std::array<double, 3> vec;
                for (int col = 0; col < 3; ++col)
                {
                    vec[col] = lattice_vectors[row][col] * scaling_factor;
                }
                res.append(QString("TV %1 %2 %3")
                               .arg(vec[0], 14, 'f', 8)
                               .arg(vec[1], 14, 'f', 8)
                               .arg(vec[2], 14, 'f', 8));
            }
        }

        res.append("\n");
        return res.join("\n");
    }

    QString Geometry::getOldVersionXYZ() const
    {
        QStringList res;
        res.append(QString::number(coordinates.size()));
        res.append("");
        for (int i = 0; i < coordinates.size(); ++i)
        {
            res.append(QString("%1 %2 %3 %4").arg(coordinates.at(i).symbol).arg(coordinates.at(i).coord[0], 14, 'f', 8).arg(coordinates.at(i).coord[1], 14, 'f', 8).arg(coordinates.at(i).coord[2], 14, 'f', 8));
        }
        if (this->isPBC)
        {
            if (this->fractional_coordinates)
            {
                res.append("0.0 0.0 0.0 F");
            }
            else
            {
                res.append("0.0 0.0 0.0 S");
            }

            for (int row = 0; row < 3; ++row)
            {
                std::array<double, 3> vec;
                for (int col = 0; col < 3; ++col)
                {
                    vec[col] = lattice_vectors[row][col] * scaling_factor;
                }
                res.append(QString("%1 %2 %3")
                               .arg(vec[0], 20, 'f', 12)
                               .arg(vec[1], 20, 'f', 12)
                               .arg(vec[2], 20, 'f', 12));
            }
        }

        return res.join("\n");
    }

    dVector3 Geometry::getCoordinate(int index) const
    {
        if (this->isPBC && this->fractional_coordinates)
        {
            dVector3 ori = this->coordinates[index].coord;
            dVector3 res{ { 0.0, 0.0, 0.0 } };
            for (int i = 0; i < 3; ++i)
            {
                for (int j = 0; j < 3; ++j)
                {
                    res[j] += this->lattice_vectors[i][j] * ori[i] * scaling_factor;
                }
            }
            return res;
        }
        else
        {
            return this->coordinates[index].coord;
        }
    }

    void Geometry::reset()
    {
        for (auto& vec : lattice_vectors)
        {
            vec.fill(0.0);
        }

        scaling_factor = 1.0;

        kpoints.m_type = ADPT::KPointsSetting::Type::Plain;
        kpoints.m_kpoints_plain.clear();

        fractional_coordinates = true;

        //Molecule
        charge = 0;
        spin = 1;

        //Common
        coordinates.clear();

        isPBC = false;
        spin_polarization = false;
        unpaired_electrons = 0.0;
    }

#define SLASHES "/\\"

    void makeAbsolutePath_local(const QDir& rootDir, QString& path)
    {
        if (path.isEmpty())
            return;
        QFileInfo info(path);

        if (info.isAbsolute())
        {
            return;
        }
        else
        {
            QString file = ADPT::expandpath(path);
            if (QFileInfo(file).exists())
            {
                path = file;
            }
            else
            {
                path = rootDir.absoluteFilePath(path);
            }
        }
    }

    QMap<QString, int> buildSystemIndexMapping(const Erepfit2_InputData& input)
    {
        QMap<QString, int> res;
        for (int i = 0; i < input.systems.size(); ++i)
        {
            res.insert(input.systems[i].uuid.toString(), i);
        }
        return res;
    }

    void UpdatePairs(QSet<QString>& skpairs, const QStringList& elements)
    {
        for (int i = 0; i < elements.size(); ++i)
        {
            for (int j = 0; j < elements.size(); ++j)
            {
                skpairs.insert(QString("%1-%2").arg(elements[i]).arg(elements[j]));
            }
        }
    }

    Erepfit2_InputData::Erepfit2_InputData(const Erepfit2_InputData& rhs)
    {
        atomicEnergy = rhs.atomicEnergy;
        potential_grids = rhs.potential_grids;
        equations = rhs.equations;
        systems = rhs.systems;
        options = rhs.options;

        if (rhs.electronic_skfiles)
        {
            electronic_skfiles = std::make_shared<ADPT::SKFileInfo>(*rhs.electronic_skfiles);
        }

        external_repulsive_potentials = rhs.external_repulsive_potentials;
        dftb_options = rhs.dftb_options;
    }

    QSet<QString> Erepfit2_InputData::required_elec_pairs() const
    {
        QSet<QString> res;
        for (int i = 0; i < systems.size(); ++i)
        {
            QStringList elements = systems[i].geometry.getElements();
            UpdatePairs(res, elements);
        }
        return res;
    }

    QStringList Erepfit2_InputData::required_elements() const
    {
        QStringList all_elements;

        for (int i = 0; i < systems.size(); ++i)
        {
            QStringList elements = systems[i].geometry.getElements();
            all_elements.append(elements);
        }

        all_elements.removeDuplicates();
        return all_elements;
    }

    void Erepfit2_InputData::makeAbsolutePath(const QDir& rootDir)
    {

        this->options.toolchain.path = ADPT::which(this->options.toolchain.path);
        makeAbsolutePath_local(rootDir, this->options.toolchain.path);

        QMutableMapIterator<QString, QString> it(this->external_repulsive_potentials);
        while (it.hasNext())
        {
            it.next();
            makeAbsolutePath_local(rootDir, it.value());
        }

        for (int i = 0; i < this->systems.size(); ++i)
        {
            makeAbsolutePath_local(rootDir, systems[i].template_input);
        }
    }

    void Erepfit2_InputData::symmetrize_external_repulsive_potentials()
    {
        QMutableMapIterator<QString, QString> it(this->external_repulsive_potentials);
        while (it.hasNext())
        {
            it.next();
            QString key = it.key();
            auto elems = key.split("-");
            QString newkey = QString("%1-%2").arg(elems[1]).arg(elems[0]);
            this->external_repulsive_potentials[newkey] = it.value();
        }
    }
}

QPair<QString, QString> potential2ElementPair(const QString& potential)
{
    QStringList arr = potential.split("-", QString::SkipEmptyParts);
    QPair<QString, QString> res;
    res.first = arr[0];
    res.second = arr[1];
    return res;
}

double getDistanceValue(double value, const QString& unit)
{
    if (unit.toLower() == "bohr")
    {
        return value;
    }
    else if (unit.toLower() == "aa")
    {
        return value / 0.529177249;
    }
    else
    {
        throw std::runtime_error("unit " + unit.toStdString() + " is not defined.");
    }
}

double getEnergyValue(double value, const QString& unit)
{
    if (unit.toLower() == "kcal/mol")
    {
        return value;
    }
    else if (unit.toLower() == "h")
    {
        return value * 627.50947;
    }
    else if (unit.toLower() == "ev")
    {
        return value * (627.50947 / 27.2113845);
    }
    else
    {
        throw std::runtime_error("unit " + unit.toStdString() + " is not defined.");
    }
}

QString writeTempFile(const QString& path, const QString& filename, const QString& content)
{
    QString final_filepath;
    QString filepath = filename;

    if (filepath.startsWith(".") || filepath.startsWith("/"))
    {
        filepath = filepath.mid(1);
    }
    else if (filepath.startsWith(".."))
    {
        filepath = filepath.mid(2);
    }

    static const char notAllowedCharsNoSubDir[] = ",^@={}[]~!?:&*\"|#%<>$\"'();`' " SLASHES;

    filepath.replace("+", "_plus");

    for (int i = 0; i < filepath.size(); ++i)
    {
        for (const char* c = notAllowedCharsNoSubDir; *c; c++)
        {
            if (filepath[i] == *c)
            {
                filepath[i] = '_';
                break;
            }
        }
    }

    QDir dir(path);
    dir.mkpath(dir.absolutePath());
    final_filepath = dir.absoluteFilePath(filepath);

    QFile file(final_filepath);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        throw std::runtime_error(final_filepath.toStdString() + " can not be open and written");
    }

    QTextStream stream(&file);
    stream << content << endl;

    return final_filepath;
}
}

QDataStream& operator<<(QDataStream& dataStream, const Erepfit2::Input::SystemElectronicData& molecule)
{
    dataStream << molecule.energy;
    dataStream << molecule.force.size();
    for (int i = 0; i < molecule.force.size(); ++i)
    {
        for (int j = 0; j < 3; ++j)
        {
            dataStream << molecule.force[i][j];
        }
    }
    dataStream << molecule.pbc;
    if (molecule.pbc)
    {
        for (int i = 0; i < 3; ++i)
        {
            for (int j = 0; j < 3; ++j)
            {
                dataStream << molecule.stress[i][j];
            }
        }
        dataStream << molecule.efermi_a;
        dataStream << molecule.efermi_b;
    }
    return dataStream;
}

QDataStream& operator>>(QDataStream& dataStream, Erepfit2::Input::SystemElectronicData& molecule)
{
    dataStream >> molecule.energy;
    int nat;
    dataStream >> nat;

    for (int i = 0; i < nat; ++i)
    {
        dVector3 vec;
        for (int j = 0; j < 3; ++j)
        {
            dataStream >> vec[j];
        }
        molecule.force.append(vec);
    }
    dataStream >> molecule.pbc;
    if (molecule.pbc)
    {
        for (int i = 0; i < 3; ++i)
        {
            for (int j = 0; j < 3; ++j)
            {
                dataStream >> molecule.stress[i][j];
            }
        }
        dataStream >> molecule.efermi_a;
        dataStream >> molecule.efermi_b;
    }
    return dataStream;
}
